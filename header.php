<!DOCTYPE html>
<html>
<head>

	<title><?php wp_title(); ?></title>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i|Montserrat:400,500,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="<?php echo $post->post_name; ?>">

	<header>
		<div class="wrapper">

			<div id="header-logo">
				<a href="<?php echo site_url('/'); ?>">
					<img class="home" src="<?php $image = get_field('header_home_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					<img class="site" src="<?php $image = get_field('header_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

					<span class="name">
						<?php echo bloginfo('name'); ?>
					</span>
				</a>
			</div>

			<a href="#" id="toggle">
				<div class="patty"></div>
			</a>

			<?php wp_nav_menu( array( 'menu' => 'Main Menu') ); ?>

		</div>
	</header>
