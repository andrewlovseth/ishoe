<?php get_header(); ?>
	
	<section id="back">
		<div class="wrapper">
			<a href="<?php echo site_url('/inspiration/'); ?>">← Back to Inspiration</a>
		</div>
	</section>

	<section id="featured-image" class="cover" style="background-image: url(<?php $image = get_the_post_thumbnail_url($post->ID, 'full'); echo $image; ?>);">
	</section>

	<section id="article-wrapper">

		<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>				

		    <article>
		    	
				<div class="article-header">
					<span class="date"><?php the_date('F j, Y'); ?></span>
					<h1><?php the_title(); ?></h1>
					<?php get_template_part('partials/byline'); ?>
				</div>

				<div class="article-body">
					<?php the_content(); ?>

					<?php get_template_part('partials/author'); ?>
			    </div>

		    </article>

		    <aside>
				<div class="share">
					<h5>Share this article</h5>

					<div class="links">
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" class="facebook" rel="external"><img src="<?php echo bloginfo('template_directory'); ?>/images/share-facebook.png" alt="Facebook" /></a>
						<a href="https://twitter.com/intent/tweet/?text=<?php echo urlencode(get_the_title()); ?>&url=<?php echo urlencode(get_permalink()); ?>" class="twitter" rel="external"><img src="<?php echo bloginfo('template_directory'); ?>/images/share-twitter.png" alt="Twitter" /></a>
						<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink()); ?>"><img src="<?php echo bloginfo('template_directory'); ?>/images/share-linkedin.png" alt="LinkedIn" /></a>
					</div>
				</div>

				<?php if(has_tag()): ?>
					<div class="tags">
						<h5>Tags in this post</h5>

						 <?php the_tags( '', ' ', '' ); ?> 
					</div>
				<?php endif; ?>

				<div class="categories">
					<ul>
					    <?php wp_list_categories( array(
					        'orderby' => 'name',
					        'exclude'    => array( 1 )
					        ) ); ?> 
					</ul>
				</div>

		    </aside>

		<?php endwhile; endif; ?>

	</section>

	<section id="pagination">
		<div class="wrapper">
			<div class="prev">
				<?php previous_post_link('<h5>Previous Post</h5><span>← %link</span>'); ?> 
			</div>

			<div class="next">
				<?php next_post_link('<h5>Next Post</h5><span>%link→ </span>'); ?> 
			</div>
		</div>
	</section>

<?php get_footer(); ?>