	<footer>
		<div class="wrapper">

			<div id="footer-logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('header_home_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>

				<div class="nav">
					<?php wp_nav_menu( array( 'menu' => 'Footer Menu') ); ?>
				</div>

			</div>


			<div id="contact">
				<h3>Get in touch</h3>
				<p><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></p>

				<div class="social">
					<h3>Get to know us</h3>

					<div class="icons">
						<a href="<?php the_field('facebook', 'options'); ?>" class="facebook" rel="external">Facebook</a>
						<a href="<?php the_field('twitter', 'options'); ?>" class="twitter" rel="external">Twitter</a>
					</div>
				</div>
			</div>


			<div id="copyright">
				<div class="note">
					<p><?php the_field('copyright', 'options'); ?></p>
				</div>

				<div class="site-credit">
					<span>Site:</span> <a href="http://andrewlovseth.com/" rel="exteral">Andrew Lovseth</a>
				</div>
			</div>

		</div>
	</footer>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>