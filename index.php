<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">

			<section id="blog-header">
				<h1>Inspiration</h1>
			</section>


			<section id="blog">

				<?php echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="9" scroll="false" transition="fade"]'); ?>


			</section>


		</div>
	</section>
	
<?php get_footer(); ?>