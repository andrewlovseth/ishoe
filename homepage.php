<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="hero">

		<?php if(have_rows('hero_slides')): while(have_rows('hero_slides')): the_row(); ?>

			<article id="<?php echo sanitize_title_with_dashes(get_sub_field('tagline')); ?>" class="cover" style="background-image: url(<?php $image = get_sub_field('image'); echo $image['url']; ?>);">	 
				<div class="wrapper">

					<div class="info">
						<h5><?php the_sub_field('tagline'); ?></h5>
						<h1><?php the_sub_field('headline'); ?></h1>
						<?php the_sub_field('deck'); ?>
					</div>

				</div>
			</article>
			
		<?php endwhile; endif; ?>


	</section>

	<section id="products">
		<div class="wrapper">

			<div class="info">
				<h5><?php the_field('products_tagline'); ?></h5>
				<h2><?php the_field('products_headline'); ?></h2>
				<?php the_field('products_deck'); ?>

				<a href="<?php the_field('products_link'); ?>" class="btn">Learn more</a>
			</div>

			<div class="image">
				<img src="<?php $image = get_field('products_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

		</div>
	</section>


	<section id="science" class="cover" style="background-image: url(<?php $image = get_field('science_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h5><?php the_field('science_tagline'); ?></h5>
				<h2><?php the_field('science_headline'); ?></h2>
				<?php the_field('science_deck'); ?>

				<a href="<?php the_field('science_link'); ?>" class="btn">Learn more</a>

				<img class="certified" src="<?php $image = get_field('science_certified_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

		</div>
	</section>


	<section id="inspiration">
		<div class="wrapper">

			<div class="info">
				<h5><?php the_field('inspiration_tagline'); ?></h5>
				<h2><?php the_field('inspiration_headline'); ?></h2>
			</div>

			<div class="posts">

				<?php
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 3
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<?php get_template_part('partials/blog-article'); ?>

				<?php endwhile; endif; wp_reset_postdata(); ?>

			</div>

			<div class="read-blog">
				<a href="<?php echo site_url('/inspiration/') ?>" class="btn">Read our blog</a>
			</div>


		</div>
	</section>


	<section class="newsletter">
		<div class="wrapper">

			<div class="newsletter-wrapper">

				<h5>Newsletter</h5>
				<h3>Sign up for our newsletter</h3>
				<p>Join us in our quest to improve the planet’s balance! We promise never to sell or share your information and you can safely unsubscribe at any time.</p>

				<?php get_template_part('partials/mailchimp-form'); ?>

			</div>

		</div>
	</section>

<?php get_footer(); ?>