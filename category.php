<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">

			<section id="blog-header">

				<div class="header">
					<em>Category</em>
					<h1><?php single_cat_title(); ?></h1>
				</div>

			</section>

			<section id="blog">

				<?php $category = get_queried_object(); echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="9" category="' . $category->slug . '" scroll="false" transition="fade"]'); ?>

			</section>

		</div>
	</section>
	
<?php get_footer(); ?>