$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Menu Toggle
	$('#toggle').click(function(){

		$('header').toggleClass('open');
		$('header .menu-main-menu-container').slideToggle(300);
		return false;
	});

	// FitVids
	$('.embed').fitVids();


	// Home Hero Slick Slider
	$('body.home section#hero').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,

	});

	$('section.text p > img').unwrap();


});
