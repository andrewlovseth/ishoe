
<div id="mc_embed_signup">
	<form id="mc-embedded-subscribe-form" class="validate" action="//iShoeBalance.us11.list-manage.com/subscribe/post?u=c072200db847a46c86cabb8d9&amp;id=830e06bdf3" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
		
		<div id="mc_embed_signup_scroll">

			<div class="mc-field-group first-name">
				<label for="mce-FNAME">First Name </label>
				<input id="mce-FNAME" class="" name="FNAME" type="text" value="" />
			</div>

			<div class="mc-field-group last-name">
				<label for="mce-LNAME">Last Name </label>
				<input id="mce-LNAME" class="" name="LNAME" type="text" value="" />
			</div>
			
			<div class="mc-field-group email">
				<label for="mce-EMAIL">Email Address <span class="asterisk">*</span></label>
				<input id="mce-EMAIL" class="required email" name="EMAIL" type="email" value="" />
			</div>

			<div class="mc-field-group how-find">
				<label for="mce-MMERGE3">How did you find us? </label>
				<select id="mce-MMERGE3" class="" name="MMERGE3">
				<option value=""></option>
				<option value="Attended Event">Attended Event</option>
				<option value="Google">Google</option>
				<option value="Houston Chronicle">Houston Chronicle</option>
				<option value="Friend">Friend</option>
				<option value="Social Media">Social Media</option>
				<option value="Other">Other</option>
				</select>
			</div>
		
			<div class="mc-field-group input-group interests"><strong>Check all that apply: </strong>
				<ul>
					<li><input id="mce-group[5925]-5925-0" name="group[5925][1]" type="checkbox" value="1" /><label for="mce-group[5925]-5925-0">I am interested in balance for myself</label></li>
					<li><input id="mce-group[5925]-5925-1" name="group[5925][2]" type="checkbox" value="2" /><label for="mce-group[5925]-5925-1">I am interested in balance for a family member</label></li>
					<li><input id="mce-group[5925]-5925-2" name="group[5925][4]" type="checkbox" value="4" /><label for="mce-group[5925]-5925-2">I am a healthcare professional</label></li>
				</ul>
			</div>
		
			<div id="mce-responses" class="clear"></div>
		
			<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
			<div style="position: absolute; left: -5000px;"><input tabindex="-1" name="b_c072200db847a46c86cabb8d9_830e06bdf3" type="text" value="" /></div>
			<div class="green"><input id="mc-embedded-subscribe" class="button" name="subscribe" type="submit" value="Subscribe" /></div>

		</div>

	</form>
</div>