<?php if(get_field('author_name')): ?>
	<span class="author">by <?php the_field('author_name'); ?></span>
<?php endif; ?>