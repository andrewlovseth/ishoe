<article class="teaser">
	<div class="image">
		<a href="<?php the_permalink(); ?>" class="cover" style="background-image: url(<?php the_post_thumbnail_url( 'medium' ); ?>);"></a>
	</div>

	<div class="post-info">
		<span class="date"><?php the_time('F j, Y'); ?></span>
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<a href="<?php the_permalink(); ?>" class="read-more">Read more</a>
	</div>
</article>