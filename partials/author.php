<?php if(get_field('author_name')): ?>

	<div class="author">

		<h5>
			<?php if(get_field('author_email')): ?>
				<a href="mailto:<?php the_field('author_email'); ?>"><?php the_field('author_name'); ?></a>
			<?php else: ?>
				<?php the_field('author_name'); ?>
			<?php endif; ?>
		</h5>

		<?php if(get_field('author_bio')): ?>
			<?php the_field('author_bio'); ?>
		<?php endif; ?>

	</div>

<?php endif; ?>