<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section id="hero">
			<div class="wrapper">

				<div class="cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
					<h1><?php the_title(); ?></h1>
				</div>

			</div>
		</section>

		<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
		 

		    <?php if( get_row_layout() == 'text' ): ?>
				
				<section class="text">
					<div class="wrapper">
					
		    			<?php the_sub_field('content'); ?>

					</div>
				</section>
				
		    <?php endif; ?>

		    <?php if( get_row_layout() == 'photo' ): ?>
				
				<section class="photo">
					<div class="wrapper">

						<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						
						<div class="caption">
			    			<?php the_sub_field('caption'); ?>
			    		</div>

					</div>
				</section>
				
		    <?php endif; ?>

		    <?php if( get_row_layout() == 'youtube' ): ?>
				
				<section class="youtube">
					<div class="wrapper">

						<div class="embed">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php the_sub_field('youtube_id'); ?>" frameborder="0" allowfullscreen></iframe>
						</div>

						<div class="caption">
			    			<?php the_sub_field('caption'); ?>
			    		</div>

					</div>
				</section>
				
		    <?php endif; ?>


		    <?php if( get_row_layout() == 'call_to_action' ): ?>
				
				<section class="call-to-action">
					<div class="wrapper">

						<div class="cta-wrapper">

							<div class="image">
								<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="info">
								<h3><?php the_sub_field('headline'); ?></h3>
								<?php the_sub_field('deck'); ?>
							</div>

							<div class="button">
								<a href="<?php the_sub_field('link'); ?>" class="btn"><?php the_sub_field('button_text'); ?></a>
							</div>

						</div>

					</div>
				</section>
				
		    <?php endif; ?>


		    <?php if( get_row_layout() == 'newsletter' ): ?>
				
				<section class="newsletter">
					<div class="wrapper">

						<div class="newsletter-wrapper">

							<h5>Newsletter</h5>
							<h3><?php the_sub_field('headline'); ?></h3>
							<?php the_sub_field('description'); ?>

							<?php get_template_part('partials/mailchimp-form'); ?>

						</div>

					</div>
				</section>
				
		    <?php endif; ?>

		 
		<?php endwhile; endif; ?>


	<?php endwhile; endif; ?>


<?php get_footer(); ?>